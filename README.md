# Product Registry Gatling

Load testing for our product registry using [Gatling](https://gatling.io/).

## Running

* Run product registry and make sure it is running on port 3000 (locally).
* Run maven task: ```mvn clean gatling:test```
* Run maven task with single test case: ```mvn clean gatling:test -Dgatling.simulationClass=org.digitalimpactalliance.RecordedSimulationForUseCase```
* Check gatling reports on ```target/gatling/recorded/${test-file-data}```

## Tweaking Tests

Tweak the setting for each test scenario to fit your need for each of the test cases.
```
  setUp(
    scn
      .inject(
        nothingFor(4 seconds),
        atOnceUsers(5),
        rampUsers(5) during (10 seconds),
        constantUsersPerSec(10) during (10 seconds),
        constantUsersPerSec(10) during (10 seconds) randomized,
        rampUsersPerSec(10) to 20 during (10 minutes),
        rampUsersPerSec(10) to 20 during (10 minutes) randomized,
        heavisideUsers(1000) during (20 seconds)
      )
      .protocols(httpProtocol)
  )
```

Add more workflow to load test by simulating the same requests done by the web browser.
```
    .exec(
      http("add_filter")
        .post("/add_filter")
        .headers(url_encoded_form_header + ("X-CSRF-Token" -> "${csrf_token}"))
        .formParam("filter_name", "use_cases")
        .formParam("filter_value", "20")
        .formParam("filter_label", "Maternal and Newborn Health")
        .resources(
          http("sdgs_count")
            .get("/sustainable_development_goals/count"),
          http("use_cases_count")
            .get("/use_cases/count"),
          http("building_blocks_count")
            .get("/building_blocks/count"),
          http("workflows_count")
            .get("/workflows/count"),
          http("products_count")
            .get("/products/count"),
          http("organizations_count")
            .get("/organizations/count"),
          http("use_cases_index")
            .get("/use_cases")
        )
    )
```
