package org.digitalimpactalliance

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulationForSdg extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:3000")
    .inferHtmlResources(
      BlackList(
        """.*\.js""",
        """.*\.css""",
        """.*\.gif""",
        """.*\.jpeg""",
        """.*\.jpg""",
        """.*\.ico""",
        """.*\.woff""",
        """.*\.woff2""",
        """.*\.(t|o)tf""",
        """.*\.png""",
        """.*detectportal\.firefox\.com.*""",
        """.*\.svg"""
      ),
      WhiteList()
    )
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader(
      "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0"
    )

  val url_encoded_form_header = Map(
    "Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
    "Origin" -> "http://localhost:3000",
    "X-Requested-With" -> "XMLHttpRequest"
  )

  val scn = scenario("RecordedSimulationForSdg")
  // SDG Page
    .exec(
      http("base_csrf_token")
        .get("/")
        .check(css("meta[name=csrf-token]", "content").saveAs("csrf_token"))
    )
    .pause(2)
    .exec(
      http("products_index")
        .get("/products")
        .resources(
          http("sdgs_count")
            .get("/sustainable_development_goals/count"),
          http("get_filters")
            .get("/get_filters"),
          http("building_blocks_count")
            .get("/building_blocks/count"),
          http("use_cases_count")
            .get("/use_cases/count"),
          http("organizations_count")
            .get("/organizations/count"),
          http("products_count")
            .get("/products/count"),
          http("workflows_count")
            .get("/workflows/count")
        )
    )
    .pause(50)
    .exec(
      http("sdgs_index")
        .get("/sustainable_development_goals")
        .resources(
          http("sdgs_count")
            .get("/sustainable_development_goals/count"),
          http("use_cases_count")
            .get("/use_cases/count"),
          http("workflows_count")
            .get("/workflows/count"),
          http("get_filters")
            .get("/get_filters"),
          http("building_blocks_count")
            .get("/building_blocks/count"),
          http("products_count")
            .get("/products/count"),
          http("organizations_count")
            .get("/organizations/count")
        )
    )
    .pause(50)
    .exec(
      http("add_filter")
        .post("/add_filter")
        .headers(url_encoded_form_header + ("X-CSRF-Token" -> "${csrf_token}"))
        .formParam("filter_name", "sdgs")
        .formParam("filter_value", "17")
        .formParam("filter_label", "2. Zero Hunger")
        .resources(
          http("sdgs_count")
            .get("/sustainable_development_goals/count"),
          http("organizations_count")
            .get("/organizations/count"),
          http("products_count")
            .get("/products/count"),
          http("use_cases_count")
            .get("/use_cases/count"),
          http("building_blocks_count")
            .get("/building_blocks/count"),
          http("workflows_count")
            .get("/workflows/count"),
          http("sdgs_index")
            .get("/sustainable_development_goals")
        )
    )
    .pause(50)
    .exec(
      http("remove_filter")
        .post("/remove_filter")
        .headers(url_encoded_form_header + ("X-CSRF-Token" -> "${csrf_token}"))
        .formParam("filter_array[0][filter_name]", "sdgs")
        .resources(
          http("organizations_count")
            .get("/organizations/count"),
          http("use_cases_count")
            .get("/use_cases/count"),
          http("sdgs_count")
            .get("/sustainable_development_goals/count"),
          http("building_blocks_count")
            .get("/building_blocks/count"),
          http("products_count")
            .get("/products/count"),
          http("workflows_count")
            .get("/workflows/count"),
          http("sdgs_index")
            .get("/sustainable_development_goals")
        )
    )

  setUp(
    scn
      .inject(
        nothingFor(4 seconds),
        atOnceUsers(5),
        rampUsers(5) during (10 seconds),
        constantUsersPerSec(10) during (10 seconds),
        constantUsersPerSec(10) during (10 seconds) randomized,
        rampUsersPerSec(10) to 20 during (10 minutes),
        rampUsersPerSec(10) to 20 during (10 minutes) randomized,
        heavisideUsers(1000) during (20 seconds)
      )
      .protocols(httpProtocol)
  )
}
