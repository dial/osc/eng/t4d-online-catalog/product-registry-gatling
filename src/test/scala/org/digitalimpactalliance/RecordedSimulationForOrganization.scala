package org.digitalimpactalliance

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulationForOrganization extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:3000")
    .inferHtmlResources(
      BlackList(
        """.*\.js""",
        """.*\.css""",
        """.*\.gif""",
        """.*\.jpeg""",
        """.*\.jpg""",
        """.*\.ico""",
        """.*\.woff""",
        """.*\.woff2""",
        """.*\.(t|o)tf""",
        """.*\.png""",
        """.*detectportal\.firefox\.com.*""",
        """.*\.svg"""
      ),
      WhiteList()
    )
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader(
      "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0"
    )

  val scn = scenario("RecordedSimulationForOrganization")
  // Organization Page
    .exec(
      http("organizations_index")
        .get("/organizations")
    )
    .exec(
      http("sdgs_count")
        .get("/sustainable_development_goals/count")
        .resources(
          http("use_cases_count")
            .get("/use_cases/count"),
          http("workflows_count")
            .get("/workflows/count"),
          http("products_count")
            .get("/products/count"),
          http("organizations_count")
            .get("/organizations/count"),
          http("building_blocks_count")
            .get("/building_blocks/count"),
          http("projects_counts")
            .get("/projects/count"),
          http("get_filters")
            .get("/get_filters")
        )
    )

  setUp(
    scn
      .inject(
        nothingFor(4 seconds),
        atOnceUsers(5),
        rampUsers(5) during (10 seconds),
        constantUsersPerSec(10) during (10 seconds),
        constantUsersPerSec(10) during (10 seconds) randomized,
        rampUsersPerSec(10) to 20 during (10 minutes),
        rampUsersPerSec(10) to 20 during (10 minutes) randomized,
        heavisideUsers(1000) during (20 seconds)
      )
      .protocols(httpProtocol)
  )
}
